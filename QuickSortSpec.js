describe("QuickSort ", function(){
    var array;
    var tab = [];

    beforeEach(function(){
        array = new QuickSort(tab, 20, 100);
        array.createAleaTab();
    });

    it("should be able to be created", function(){
        expect(array).toBeDefined();
        expect(array.tab).toBe(tab);
        expect(array.maxNumber).toBe(100);
        expect(array.tabSize).toBe(20);
        expect(array.tab[19]).toEqual(jasmine.any(Number));
    });

    describe("can swap tab values", function(){
        it("if the indexes are not out of bound", function(){
            expect(function(){
                array.swap(0,750);
            }).toThrowError();
        });
        it("and if the indexes are OK", function(){
            var val0 = array.tab[0];
            var val7 = array.tab[7];
            array.swap(0,7);
            expect(array.tab[0]).toEqual(val7);
            expect(array.tab[7]).toEqual(val0);
        });

    });

    describe("can sort table values", function(){
        it("by calling partition method",function(){
            spyOn(array,"partition");
            array.sort(0,19);
            expect(array.partition).toHaveBeenCalled();
        });
        it("with a fake alea tab", function(){
            var expectedTab = [7,5,9,10,2,1];
            var expectedSortedTab = [1,2,5,7,9,10];
            spyOn(array,"createAleaTab").and.callFake(function(){
                array.tab= expectedTab;
            });
            array.createAleaTab();
            expect(array.tab).toEqual(expectedTab);
            array.sort(0,5);
            expect(array.tab).toEqual(expectedSortedTab);
        });
    });


});