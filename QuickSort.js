class QuickSort{
    constructor(tab, tabSize, maxNumber){
        this.tab = tab;
        this.tabSize = tabSize;
        this.maxNumber = maxNumber;
    };

    createAleaTab(){
        var i = 0;
        for(i=0; i< this.tabSize; i++){
            this.tab.push(Math.floor(Math.random() * 100));
        };
    };

    swap(i,j){
        if(i > this.tabSize || j> this.tabSize){
            throw new Error();
        } else{
            var temp = this.tab[i];
            this.tab[i] = this.tab[j];
            this.tab[j] = temp;
        }
    };

    printArray(){
        this.tab.array.forEach(element => {
            print(element);
        });
    };

    sort(left, right){
        var len = this.tab.length, 
        pivot,
        partitionIndex;
     
     
       if(left < right){
         pivot = right;
         partitionIndex = this.partition(pivot, left, right);
         
        //sort left and right
        this.sort(left, partitionIndex - 1);
        this.sort(partitionIndex + 1, right);
       };
    };
    
    partition(pivot, left, right){
        var pivotValue = this.tab[pivot],
            partitionIndex = left;
     
        for(var i = left; i < right; i++){
         if(this.tab[i] < pivotValue){
           this.swap(i, partitionIndex);
           partitionIndex++;
         }
       }
       this.swap(right, partitionIndex);
       return partitionIndex;
    };
};